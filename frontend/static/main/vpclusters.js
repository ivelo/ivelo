import {set_colorscale} from "./colorscale.js";
import {buildbox} from "./box.js";

export function fillrunnoise(){
    console.log("Filling selects...");
    var select = document.getElementById('param_select');
    for (var a in select.options) { select.options.remove(0); }
    let scans = ['nobeam_average'];
    let i = 0;
    while (i < scans.length){
        var opt = document.createElement('option');
        //opt.value = item;
        opt.innerHTML = scans[i];
        select.appendChild(opt);
        i++;
    }
}

export function buildrunnoise() {
    const csrftoken = document.querySelector('[name=csrfmiddlewaretoken]').value;
    buildbox(csrftoken, '/api/clusters', 'clusters');
    
    document.getElementById('param_select').onchange = function(){
      console.log('Will fill the dashboard.');
      //console.log(document.querySelector(".browser recipelist-selected"));
      var runid = document.querySelector(".recipelist-selected").childNodes[1].innerText;
      console.log('runid is ' + runid);
      var stype = document.getElementById("param_select").value;
      var min, max;
      if (stype == 'nobeam_average'){
        min = 0;
        max = 1000;}

      console.log(runid);
      $.ajax({
        url: 'http://10.128.124.243:8003/api/clustersasic?id=' + runid,// + '&subid=' + subid_str,
        type: 'GET',
        dataType: 'json',
        headers: {'X-CSRFToken':  csrftoken},//'http://localhost:8003/api/recipe/hw'},
        crossDomain: true,
        crossOrigin: true,
        success: function(data, textStatus) {
            console.log('Success, filling in Select View', data);
            for(let column = 1; column <= 52; column++)
            {
              for(let row = 0; row < 12; row++)
              {
                //console.log(data);
                let asic = (row) + (column -1) * 12;
                var p = data[asic][stype];
                let columnhalf = column; //- 1;
                if ((column - 1) % 2 == 0) columnhalf = (column - 1)/2 + 1
                else if ((column - 1) % 2 == 1) columnhalf = (column)/2 + 1 + 25

                var currentcell = document.querySelector(`[data-row='${row}'][data-column='${columnhalf}']`);
                currentcell.innerHTML = `<span class="module_quality">${p}</span><br />`;
                currentcell.style.background = set_colorscale('jet', p, min, max);
              }
            }
        },
        error: function(xhr, textStatus, errorThrown){
            console.log('fail');
        }
    });
    };
  }