from django.db import models
"""
class Recipe(models.Model):
    #type of recipe
    kind = models.CharField(max_length = 30, default = 'DEFAULT')
    #date of creation
    date = models.DateField()
    #comment displayed in the recipe viewer
    comment = models.CharField(max_length = 255)

    class Meta:
        ordering = ["date"]
        app_label = 'backend'

class AsicRecipe(models.Model):
    #hardware settings (DAC, phase, ...)
    path_chip_settings = models.CharField(max_length = 255)
    #pixel settings (mask, TP, trim)
    path_settings = models.CharField(max_length = 255)

    #identification
    asic = models.PositiveIntegerField()

    #basic info to get without looping over actual recipes
    mask_total = models.PositiveIntegerField()
    global_threshold = models.PositiveIntegerField()
    recipe = models.ManyToManyField(Recipe)

    class Meta:
        ordering = ["asic"]
        app_label = 'backend'
"""