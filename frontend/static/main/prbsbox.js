import {set_colorscale} from "./colorscale.js";
import {buildbox} from "./box.js";
import {prbslinkmapping} from "./prbsmap.js";

export function fillprbs(){
    console.log("Filling selects...");
    var select = document.getElementById('param_select');
    for (var a in select.options) { select.options.remove(0); }
    let scans = ['error', '-log10(ratio)'];
    let i = 0;
    while (i < scans.length){
        var opt = document.createElement('option');
        //opt.value = item;
        opt.innerHTML = scans[i];
        select.appendChild(opt);
        i++;
    }
}

export function buildprbsbox() {
    const csrftoken = document.querySelector('[name=csrfmiddlewaretoken]').value;
    buildbox(csrftoken, '/api/prbs', 'prbs');
    
    document.getElementById('param_select').onchange = function(){
      console.log('Will fill the dashboard.');
      var recipe_id = document.querySelectorAll(".recipelist-selected")[0].childNodes[1].innerText;
      var stype = document.getElementById("param_select").value;
      var min, max;
      if (stype == 'error'){
        min = 0;
        max = 200;}
      if (stype == '-log10(ratio)'){
        min = 10.5;
        max = 1;
      }

      console.log(recipe_id);
      $.ajax({
        url: 'http://10.128.124.243:8003/api/prbs?id=' + recipe_id,// + '&subid=' + subid_str,
        type: 'GET',
        dataType: 'json',
        headers: {'X-CSRFToken':  csrftoken},//'http://'},
        crossDomain: true,
        crossOrigin: true,
        success: function(data, textStatus) {
            console.log('Success, filling in Select View', data);
            for(let column = 1; column <= 52; column++)
            {
              for(let row = 0; row < 20; row++)
              {
                let columnhalf = column; //- 1;
                if ((column - 1) % 2 == 0) columnhalf = (column - 1)/2 + 1
                if ((column - 1) % 2 == 1) columnhalf = (column)/2 + 1 + 25
                //let rowid = prbslinkmapping[row];
                
                let rowid = (row) + (column -1) * 20;
                //if (data[linkid]['module'] != column - 1) {console.log(data[linkid]['module'], column - 1); column++; } 
                //linkid = (rowid) + (column -1) * 20;

                var p;
                if (stype == '-log10(ratio)'){
                  //if (data[rowid]['error'] == 0) p = 0;
                  //console.log(data[rowid]['error']/data[rowid]['nbits']);
                  p = - Math.log10(data[rowid]['error']/data[rowid]['nbits']).toFixed(1);}
                  //console.log(p, data[rowid]['nbits'], data[rowid]['error'] )}
                
                else if (stype == 'error'){
                  p = data[rowid][stype];}

                var linkid = data[rowid]['link'];
                //console.log(linkid, rowid);
                if (linkid !== null) if (linkid > 9) linkid -= 2;
                //console.log(linkid)
                if (linkid == null) linkid = row;
                //console.log(linkid, row);
                var currentcell = document.querySelector(`[prbs-row='${linkid}'][prbs-column='${columnhalf}']`);
                currentcell.innerHTML = `<span class="module_quality">${p}</span><br />`;
                currentcell.style.background = set_colorscale('jet', p, min, max);
              }
            }
        },
        error: function(xhr, textStatus, errorThrown){
            console.log('fail');
        }
    });
    };
  }