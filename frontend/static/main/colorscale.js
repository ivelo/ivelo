
export var available_cs = ['jet', 'reds'];

export function set_colorscale(cstype, index, min = 0, max = 255, adjust = false){
    if (cstype == 'jet')
        return jet(index, min, max, adjust);
    else if (cstype == 'reds')
        return reds(index, min, max, adjust);
    else
        return false;
}

function jet(index, min = 0, max = 255, adjust = false) {
    update_cs_settings(min, max);
    if (index == null){ return "rgb(80, 80, 80)";}
    if (adjust == true){ while(index > 255) index /= 2;
        update_cs_settings(min, max);}
    index = (index - min)*255/(max - min);
    if (index > 255) index = 255;
    let rgb;
    if (index < 64) rgb = "rgb(0," + index*4 + ",255)";
    if (index < 128 && index >= 64) rgb = "rgb(0,255," + (255 - (index-64)*4)+ ")";
    if (index < 192 && index >= 128) rgb = "rgb(" + (index-128)*4 + ",255,0)";
    if (index >= 192) rgb = "rgb(255," + (255 - (index-192)*4) +",0)";
    return rgb;
}

function reds(index, min = 0, max = 255) {
    update_cs_settings(min, max);
    index = (index - min)*255/(max - min);
    if (index > 255) index = 255;
    let rgb;
    rgb = "rgb(255," + index +"," + index + ")";
    return rgb;
}

export function update_from_cs_settings(){
    let cs = document.getElementById('colorscale_select').value;
    let min = document.getElementById('colorscale_input1').value;
    let max = document.getElementById('colorscale_input2').value;
    var tablecells = document.querySelectorAll(".asiccell");
    for (var i = 0; i < tablecells.length; i++){
        if (tablecells[i].innerHTML !== "")
            //console.log(tablecells, tablecells[i]);
            tablecells[i].style.background = set_colorscale(cs, tablecells[i].textContent, min, max, false);
        }
}

function update_cs_settings(low, high){
    document.getElementById('colorscale_input1').value = low;
    document.getElementById('colorscale_input2').value = high;
}