import {set_colorscale} from "./colorscale.js";
import {buildbox} from "./box.js";

export function fillchiprecipe(){
  console.log("Filling selects...");
  var select = document.getElementById('param_select');
  for (var a in select.options) { select.options.remove(0); }
  let scans = ['dac_IDISC', 'dac_IKRUM', 'dac_IPIXELDAC', 'dac_IPREAMP',
               'dac_VCASDISC', 'dac_VFBK', 'dac_VINCAS', 'dac_VPREAMP_CAS',
               'dac_REC_BIAS', 'dac_VTPFINE', 'dac_VTPCOARSE', 'dac_threshold',
               'pll_phase', 'router_mode', 'tp_enable', 'tp_num',
               'tp_on', 'tp_off', 'tp_phase', 'tp_dig',
               'fsm_mode', 'gwt_links'];
  let i = 0;
  while (i < scans.length){
      var opt = document.createElement('option');
      opt.innerHTML = scans[i];
      select.appendChild(opt);
      i++;
  }
}

export function buildrecipebox() {
  const csrftoken = document.querySelector('[name=csrfmiddlewaretoken]').value;
  //Solution inspired by Stack Overflow 15477527
  buildbox(csrftoken, '/api/recipe/hw', 'hwrecipe');
  
  //getPaginationNumbers();
  //setCurrentPage(1);
  document.getElementById('param_select').onchange = function(){
    var recipe_id = document.querySelectorAll(".recipelist-selected")[0].childNodes[1].innerText;
    var settings = document.getElementById("param_select").value;
    $.ajax({
      url: 'http://10.128.124.243:8003/api/recipe/subrecipe/hw?id=' + recipe_id,// + '&subid=' + subid_str,
      type: 'GET',
      dataType: 'json',
      headers: {'X-CSRFToken':  csrftoken},//'http://localhost:8003/api/recipe/hw'},
      crossDomain: true,
      crossOrigin: true,
      success: function(data, textStatus) {
          //console.log('Success, filling in Select View', data);
          for(let row = 0; row < 12; row++)
          {
            for(let column = 1; column <= 52; column++)
            {
              let asic = (row) + (column -1) * 12;
              //console.log(data[asic].data);
              var hwrecipe = JSON.parse(data[asic].data);
              //console.log(hwrecipe);
              //let asic_obj = this._velomap.GetAsic(asic);
              let columnhalf = column; //- 1;
              if ((column - 1) % 2 == 0) columnhalf = (column - 1)/2 + 1
              else if ((column - 1) % 2 == 1) columnhalf = (column)/2 + 1 + 25
              var currentcell = document.querySelector(`[data-row='${row}'][data-column='${columnhalf}']`);
              currentcell.innerHTML = `<span class="module_quality">${hwrecipe[settings]}</span><br />`;
              //var rgb = "rgb(" + hwrecipe[settings] + ",150,150)";
              //console.log(jet(hwrecipe[settings]));
              currentcell.style.background = set_colorscale('jet', hwrecipe[settings]);
            }
          }
      },
      error: function(xhr, textStatus, errorThrown){
          console.log('fail');
      }
  });
  };
}

//export function deleterecipebox(){
//  $("#recipelist tr").remove();
//  $("#pagination-numbers button").remove();
//  console.log('Deleting recipebox...');
  //document.querySelector(".recipelist").remove();
  //document.querySelector(".pagination-numbers")
//}

//window.addEventListener("load", buildrecipebox(event), false);
