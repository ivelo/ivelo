import {set_colorscale} from "./colorscale.js";
import {buildbox} from "./box.js";

export function fillta(){
    console.log("Filling selects...");
    var select = document.getElementById('param_select');
    for (var a in select.options) { select.options.remove(0); }
    let scans = ['bxid_mean', 'bxid_std', 'bxid_min', 'bxid_max'];
    let i = 0;
    while (i < scans.length){
        var opt = document.createElement('option');
        //opt.value = item;
        opt.innerHTML = scans[i];
        select.appendChild(opt);
        i++;
    }
}

export function buildtabox() {
    const csrftoken = document.querySelector('[name=csrfmiddlewaretoken]').value;
    buildbox(csrftoken, '/api/ta', 'ta');
    
    document.getElementById('param_select').onchange = function(){
      console.log('Will fill the dashboard.');
      //console.log(document.querySelector(".browser recipelist-selected"));
      var recipe_id = document.querySelector(".recipelist-selected").childNodes[1].innerText;
      var stype = document.getElementById("param_select").value;
      var min, max;
      if (stype == 'bxid_mean'){
        min = -0.02;
        max = 0.2;}
      if (stype == 'bxid_std'){
        min = 0;
        max = 3;}
      if (stype == 'bxid_min'){
        min = 0;
        max = -6;}
      if (stype == 'bxid_max'){
        min = 0;
        max = 5;}

      console.log(recipe_id);
      $.ajax({
        url: 'http://10.128.124.243:8003/api/ta?id=' + recipe_id + '&type=' + stype,// + '&subid=' + subid_str,
        type: 'GET',
        dataType: 'json',
        headers: {'X-CSRFToken':  csrftoken},//'http://localhost:8003/api/recipe/hw'},
        crossDomain: true,
        crossOrigin: true,
        success: function(data, textStatus) {
            console.log('Success, filling in Select View', data);
            for(let column = 1; column <= 52; column++)
            {
              for(let row = 0; row < 12; row++)
              {
                let asic = (row) + (column -1) * 12;
                var p = data[asic][stype];
                let columnhalf = column; //- 1;
                if ((column - 1) % 2 == 0) columnhalf = (column - 1)/2 + 1
                else if ((column - 1) % 2 == 1) columnhalf = (column)/2 + 1 + 25

                var currentcell = document.querySelector(`[data-row='${row}'][data-column='${columnhalf}']`);
                currentcell.innerHTML = `<span class="module_quality">${p}</span><br />`;
                currentcell.style.background = set_colorscale('jet', p, min, max);
              }
            }
        },
        error: function(xhr, textStatus, errorThrown){
            console.log('fail');
        }
    });
    };
  }