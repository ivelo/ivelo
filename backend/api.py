#defines API functions
import requests 

API_HOST = 'http://127.0.0.1:8003'
STORAGE = '/calib/velo/.recipe/'

def get(apath):
    session = requests.Session()
    session.trust_env = False
    content = session.get(
        "{}{}".format(API_HOST, '/api/recipe/' + apath)
    )
    content.raise_for_status()
    return content.json()
