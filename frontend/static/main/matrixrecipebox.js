import {getcolorrange} from "./colorrange.js";
import {set_colorscale} from "./colorscale.js";
import {buildbox} from "./box.js";

export function fillmatrixrecipe(){
  console.log("Filling selects...");
  var select = document.getElementById('param_select');
  for (var a in select.options) { select.options.remove(0); }
  let scans = ['masks'];
  let i = 0;
  while (i < scans.length){
      var opt = document.createElement('option');
      opt.innerHTML = scans[i];
      select.appendChild(opt);
      i++;
  }
}

export function buildmatrixrecipebox() {
  const csrftoken = document.querySelector('[name=csrfmiddlewaretoken]').value;
  //Solution inspired by Stack Overflow 15477527
  buildbox(csrftoken, '/api/recipe/matrix', 'matrixrecipe');
  
  //getPaginationNumbers();
  //setCurrentPage(1);
  document.getElementById('param_select').onchange = function(){
    console.log('Will fill the dashboard.');
    var recipe_id = document.querySelectorAll(".recipelist-selected")[0].childNodes[1].innerText;
    var settings = document.getElementById("param_select").value;
    $.ajax({
      url: 'http://10.128.124.243:8003/api/recipe/subrecipe/matrix?id=' + recipe_id,// + '&subid=' + subid_str,
      type: 'GET',
      dataType: 'json',
      headers: {'X-CSRFToken':  csrftoken},//'http://localhost:8003/api/recipe/hw'},
      crossDomain: true,
      crossOrigin: true,
      success: function(data, textStatus) {
          //console.log('Success, filling in Select View', data);
          for(let row = 0; row < 12; row++)
          {
            for(let column = 1; column <= 52; column++)
            {
              let asic = (row) + (column -1) * 12;
              //console.log(data[asic].data);
              var masks = JSON.parse(data[asic].masks);
              //console.log(hwrecipe);
              //let asic_obj = this._velomap.GetAsic(asic);
              let columnhalf = column; //- 1;
              if ((column - 1) % 2 == 0) columnhalf = (column - 1)/2 + 1
              else if ((column - 1) % 2 == 1) columnhalf = (column)/2 + 1 + 25
              var currentcell = document.querySelector(`[data-row='${row}'][data-column='${columnhalf}']`);
              currentcell.innerHTML = `<span class="module_quality">${masks}</span><br />`;
              //var rgb = "rgb(" + hwrecipe[settings] + ",150,150)";
              //console.log(jet(hwrecipe[settings]));
              var [low, high] = getcolorrange('equalisation', 'masks');
              currentcell.style.background = set_colorscale('jet', masks, low, high, false);
            }
          }
      },
      error: function(xhr, textStatus, errorThrown){
          console.log('fail');
      }
  });
  };
}

//export function deleterecipebox(){
//  $("#recipelist tr").remove();
//  $("#pagination-numbers button").remove();
//  console.log('Deleting recipebox...');
  //document.querySelector(".recipelist").remove();
  //document.querySelector(".pagination-numbers")
//}

//window.addEventListener("load", buildrecipebox(event), false);
