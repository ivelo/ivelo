import {
    VeloMap,
}
from "./velomap.js";

import {
    VeloMapBoard,
}
from "./velomapboard.js";

const APP = {};

window.onload = function()
{
    APP.velomap = new VeloMap();
    APP.display = new VeloMapBoard(APP.velomap, "velomap", "infoboxbackground", "infobox");
}
