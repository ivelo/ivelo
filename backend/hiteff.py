from django.shortcuts import render, redirect
from django.contrib import auth
#from rest_framework.authtoken.models import Token
#from django.http import JsonResponse
#from .utils import get_current_git_commit

from bokeh.plotting import figure, save, output_file
from bokeh.embed import components
from bokeh.models import Quad, NumericInput, LinearColorMapper, ColumnDataSource, Select, CustomJS, Slider
from bokeh.models import LogScale, LinearScale, ColorBar, RangeSlider, Slope
from bokeh.layouts import column, row
from bokeh.palettes import diverging_palette, Sunset10

from bokeh.transform import linear_cmap
from bokeh.util.hex import hexbin

#from backend.setloader import setload, saveset
#from bokeh.models.widgets import RangeSlider
import numpy as np
from copy import copy

#from backend.api import get

from datetime import datetime
import requests
import uproot

def velofactorize(vpid):
    return int(vpid/12), int((vpid%12)/3), int((vpid%12)%3)

def _minihist_range():
    code =  """                             
            source.data.l_x = [cb.value[0]];
            source.data.r_x = [cb.value[1]];
            source.change.emit(); 
            """
    return code

def _common_setup_mainplot(p):
    p.legend.label_text_font_size = '8pt'
    p.xaxis.axis_label = 'Column ID'
    p.xaxis.axis_label_text_font_size = '8pt'
    p.yaxis.axis_label = 'Row ID'
    p.yaxis.axis_label_text_font_size = '8pt'
    p.legend.padding = 1
    p.legend.spacing = 2

def _plot_and_pitch():
    code = """                                        
            const R = sq_pitch.value;
            const squish = Math.pow(2, sq_pitch.value-1);
            var newsize = 256/squish;
            var image = source.data.image_now;
            for (let i=0; i<newsize; i++){
                for (let j=0; j<newsize; j++){
                    var xmulti = i*256*squish;
                    var ymulti = j*squish;
                    var getmean = 0;
                    var counters = 0;
                    for (let x=0; x<squish; x++){
                        for (let y=0; y<squish; y++){
                            if (!isNaN(source.data.image_const[0][xmulti + x*256 + ymulti + y])){
                                getmean += source.data.image_const[0][xmulti + x*256 + ymulti + y];
                                counters += 1;
                            }
                        }
                    }
                    
                    getmean /= counters;
                    for (let x=0; x<squish; x++){
                        for (let y=0; y<squish; y++){
                            image[0][xmulti + x*256 + ymulti + y] = getmean;
                        }
                    }
                }
            }
            source.data.image = image; 
            source.change.emit();
            """
    return code


def get_bokeh_nstd(run):#vpid, dbid, type, scan):
    #mod, tile, asic = velofactorize(int(vpid))

    base2run = int(np.floor(run / 10000.0) * 10000)
    base3run = int(np.floor(run / 1000.0) * 1000)
    
    hitpass = uproot.open(f'/hist/Savesets/ByRun/VeloTrackMon/{base2run}/{base3run}/VeloTrackMon-run{run}.root')['VPHitEfficiencyMonitorSensor_116']['hitPass']#[entity]
    hitpass = hitpass.to_numpy()
    hittotal = uproot.open(f'/hist/Savesets/ByRun/VeloTrackMon/{base2run}/{base3run}/VeloTrackMon-run{run}.root')['VPHitEfficiencyMonitorSensor_116']['hitTotal']#[entity]
    hittotal = hittotal.to_numpy()

    hitpass = hitpass[0][256:512].T
    hittotal = hittotal[0][256:512].T

    print(np.sum(hittotal))
    hittotal[hittotal == 0] = np.nan
    #hitpass[hitpass == 0] 
    #print(np.sum(hittotal, where = np.isscalar(hittotal)))
    
    ratio = np.divide(hitpass, hittotal, where = hittotal!= 0, out = np.zeros([256,256]))
    #print(hitpass[0]) #counts
    #print(hitpass[1]) #0-768
    #print(hitpass[2]) #0-255
    #print(hitpass[0].shape)
    #send GET request to the db
    #fpath = _get('/api/equalisation/custom?type=' + type + '&dbid=' +dbid+ '&vpid=' +vpid+ '&param=path')['path']
    datamap = ratio
    print(datamap, datamap.shape)
    #datamap = np.loadtxt(fpath + 'Module' + str(mod).zfill(2) +  
    #    '_VP' + str(tile) + '-' + str(asic) + '_' + scan + '_' + 'std.csv', delimiter = ',')
    
    p = figure(width=445, height=340, x_range = (0,255), y_range = (0,255))
    sq_pitch = Slider(start=1, end=6, value=1, step=1, title="Square pitch", width = 160, show_value = False)
    
    src = ColumnDataSource(data = dict(image_now = [datamap], image_const = [datamap]))
    c_mapper = LinearColorMapper(palette = 'Viridis256')

    r = p.image(image = 'image_now', source = src, x = 0, y = 0, dw = 256, dh = 256, color_mapper = c_mapper)
    cb_low = RangeSlider(start=0, end=1, value= (0,1), 
        step=0.01, title="Colorbar scale", width = 160, show_value = True)

    cb_low.js_link('value', r.glyph.color_mapper, 'low', attr_selector=0)
    cb_low.js_link('value', r.glyph.color_mapper, 'high', attr_selector=1)

    graph = figure(width=220, height=140, x_range = (-0.2,1.2), y_range = (0,60000), y_axis_type = 'log')
    bins = np.linspace(-0, 1, 101)
    hist, edges = np.histogram(datamap, bins=bins)
    src_hist = ColumnDataSource(dict(x=edges[1:], y=hist))
    graph.step(x = 'x', y = 'y', line_color = '#000000', source = src_hist)
    graph.yaxis.visible = False
    graph.xaxis.axis_label_text_font_size = '8pt'
    graph.xaxis.axis_label = 'Average (todo)'
    print(graph.x_range.start, graph.y_range.end)
    l_x, r_x = 0, 1
    l_src = ColumnDataSource(dict(l_x=[l_x], r_x = [r_x]))
    l_graph = graph.vspan(x = 'l_x', source = l_src, line_color = 'blue')
    r_graph = graph.vspan(x = 'r_x', source = l_src, line_color = 'red')
    l_box = Quad(left = 'l_x', right = 'r_x', bottom = 0, top = 60000, fill_color = 'blue', fill_alpha = 0.05)
    graph.add_glyph(l_src, l_box)
    callback_range = CustomJS(args=dict(source=l_src, cb = cb_low), code = _minihist_range())
    cb_low.js_on_change('value', callback_range)

    p.legend.padding = 1
    p.legend.spacing = 2
    _common_setup_mainplot(p)
    
    cb = r.construct_color_bar(width = 15)
    p.add_layout(cb, 'right')

    callback = CustomJS(args=dict(source=src, sq_pitch = sq_pitch), code = _plot_and_pitch())
    sq_pitch.js_on_change('value', callback)

    output_file(filename='../frontend/templates/main/bokeh.html', title='Static HTML file')
    path = save(row(p, column(sq_pitch, cb_low, graph, width = 200)))

if __name__ == '__main__':
    get_bokeh_nstd(run = 291511)