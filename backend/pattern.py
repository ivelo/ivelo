import uproot
import numpy as np
import os
from datetime import datetime
import sys
from matplotlib import pyplot as plt

def collect(run, dacname, setname, catalog, extra = None):

    base2run = int(np.floor(run / 10000.0) * 10000)
    base3run = int(np.floor(run / 1000.0) * 1000)
    
    pcounts = np.zeros([624])
    dac = np.zeros([624])

    set = uproot.open(f'/hist/Savesets/ByRun/{setname}/{base2run}/{base3run}/VeloMon-run{run}.root')[catalog]#[entity]
    for mod in range(52):
        for sensor in range(4):
            name = f'VPClusterMapOnMod{mod}Sens{sensor}'
            data = set[name].counts().T
            for asic in range(3): 
                pcounts[mod*12 + sensor*3 + asic] = np.sum(data[6::16, 256*asic: 256*asic + 256])
                modname = 'Module' + str(mod).zfill(2)
                rpath = f'/calib/velo/chip03may/DEFAULT/{modname}/{modname}_VP{sensor}-{asic}.dat'

                fin = open(rpath, 'r')
                fdata = fin.read().splitlines()
                for i in range(len(fdata)):
                    if dacname in fdata[i]:
                        dac[mod*12 + sensor*3 + asic] = int(fdata[i].split('=')[-1])
                if extra == 'halfwayreduction':
                    rpath = f'/calib/velo/chip09may/QA/{modname}/{modname}_VP{sensor}-{asic}.dat'
                    fin = open(rpath, 'r')
                    fdata = fin.read().splitlines()
                    for i in range(len(fdata)):
                        if dacname in fdata[i]:
                            dac[mod*12 + sensor*3 + asic] -= int(fdata[i].split('=')[-1])
                            #if int(fdata[i].split('=')[-1]) > 2000:
                            #    print(mod, sensor, asic)
                if extra == 'sigma':
                    rpath = f'/calib/velo/equalisation/equalis_NewPanel/Summer23/{modname}/normal/{modname}_VP{sensor}-{asic}_scan0_rate.csv'
                    fdata = np.loadtxt(rpath, delimiter = ',')
                    dac[mod*12 + sensor*3 + asic] = np.nanmean(fdata)#fdata[:, 1::2] - fdata[:, ::2])

    fig, ax = plt.subplots(figsize = [6,6])
    if catalog == 'VPClusterMonitors_NoBeam':
        color = 'blue'
    else:
        color = 'red'

    ax.scatter(dac, pcounts, color = color, s = 8, label = '624 ASICs')

    ax.legend()
    ax.set_xlabel(f'{dacname}')
    if extra == 'halfwayreduction':
        ax.set_xlabel(f'{dacname} used in 2023 - optimized')
    elif extra == 'sigma':
        ax.set_xlabel(f'Sigma of the noise in Summer23')
    ax.set_yscale('log')
    ax.set_ylabel(f'6-22-38-... rows, aggregated sum of hits')
    ax.set_title(f'Run {run} {catalog}')
    ax.grid()
    which = catalog.split('_')[-1]
    plt.savefig(f'/home/velo_user/revisited_equalisation/pattern_corr_{run}_{which}_vs_{dacname}{extra}.png')
    #plt.show()

def collect(run, catalog):
    base2run = int(np.floor(run / 10000.0) * 10000)
    base3run = int(np.floor(run / 1000.0) * 1000)
    return uproot.open(f'/hist/Savesets/ByRun/VeloMon/{base2run}/{base3run}/VeloMon-run{run}.root')[catalog]

def plot(x, y, catalog, name, title = None, xlabel = None, ylabel = None):
    fig, ax = plt.subplots(figsize = [6,6])
    if catalog == 'VPClusterMonitors_NoBeam':
        color = 'blue'
    else:
        color = 'red'

    ax.scatter(x, y, color = color, s = 8, label = 'All ASICs')
    ax.scatter(x[21*12: 22*12], y[21*12: 22*12], color = 'green', s = 52, label = 'M21', marker = 'x')
    ax.legend(fontsize = 11)
    ax.set_xlabel(f'{xlabel}', fontsize = 15)
    ax.set_ylabel(f'{ylabel}', fontsize = 15)
    #if extra == 'halfwayreduction':
    #    ax.set_xlabel(f'{dacname} used in 2023 - optimized')
    #elif extra == 'sigma':
    #    ax.set_xlabel(f'Sigma of the noise in Summer23')
    ax.set_yscale('log')
    ax.set_title(title, fontsize = 16)
    ax.grid()
    which = catalog.split('_')[-1]
    plt.savefig(f'/home/velo_user/revisited_equalisation/{name}.png')
    plt.close()

def dac_vs_hits(run, catalog, dacname):
    set = collect(run, catalog)
    x, y = np.zeros([624]), np.zeros([624])
    for mod in range(52):
        for sensor in range(4):
            name = f'VPClusterMapOnMod{mod}Sens{sensor}'
            data = set[name].counts().T
            for asic in range(3): 
                y[mod*12 + sensor*3 + asic] = np.sum(data[6::16, 256*asic: 256*asic + 256])
                modname = 'Module' + str(mod).zfill(2)
                rpath = f'/calib/velo/chip03may/DEFAULT/{modname}/{modname}_VP{sensor}-{asic}.dat'

                fin = open(rpath, 'r')
                fdata = fin.read().splitlines()
                for i in range(len(fdata)):
                    if dacname in fdata[i]:
                        x[mod*12 + sensor*3 + asic] = int(fdata[i].split('=')[-1])
    ifbeam = catalog.split('_')[-1]
    plot(x, y, catalog, f'dac_{run}_{ifbeam}_{dacname}', title = f'Run {run} {ifbeam}', 
         xlabel = dacname, ylabel = '6-22-38-... rows, aggregated sum of hits')

def dac_halfway_vs_hits(run, catalog, dacname):
    set = collect(run, catalog)
    x, y = np.zeros([624]), np.zeros([624])
    for mod in range(52):
        for sensor in range(4):
            name = f'VPClusterMapOnMod{mod}Sens{sensor}'
            data = set[name].counts().T
            for asic in range(3): 
                y[mod*12 + sensor*3 + asic] = np.sum(data[6::16, 256*asic: 256*asic + 256])
                modname = 'Module' + str(mod).zfill(2)
                rpath = f'/calib/velo/chip03may/DEFAULT/{modname}/{modname}_VP{sensor}-{asic}.dat'

                fin = open(rpath, 'r')
                fdata = fin.read().splitlines()
                for i in range(len(fdata)):
                    if dacname in fdata[i]:
                        x[mod*12 + sensor*3 + asic] = int(fdata[i].split('=')[-1])
                rpath = f'/calib/velo/chip09may/QA/{modname}/{modname}_VP{sensor}-{asic}.dat'
                fin = open(rpath, 'r')
                fdata = fin.read().splitlines()
                for i in range(len(fdata)):
                    if dacname in fdata[i]:
                        x[mod*12 + sensor*3 + asic] -= int(fdata[i].split('=')[-1])

    ifbeam = catalog.split('_')[-1]
    plot(x, y, catalog, f'dac_halfway_{run}_{ifbeam}_{dacname}', title = f'Run {run} {ifbeam}', 
         xlabel = dacname, ylabel = '6-22-38-... rows, aggregated sum of hits')

def equal_vs_hits(run, catalog, param, rows = 'all'):
    set = collect(run, catalog)
    x, y = np.zeros([624]), np.zeros([624])
    for mod in range(52):
        for sensor in range(4):
            name = f'VPClusterMapOnMod{mod}Sens{sensor}'
            data = set[name].counts().T
            for asic in range(3): 
                y[mod*12 + sensor*3 + asic] = np.sum(data[6::16, 256*asic: 256*asic + 256])
                modname = 'Module' + str(mod).zfill(2)
                #rpath = f'/calib/velo/chip03may/DEFAULT/{modname}/{modname}_VP{sensor}-{asic}.dat'

                #fin = open(rpath, 'r')
                #fdata = fin.read().splitlines()
                #for i in range(len(fdata)):
                #    if dacname in fdata[i]:
                #        x[mod*12 + sensor*3 + asic] = int(fdata[i].split('=')[-1])
                rpath = f'/calib/velo/equalisation/equalis_NewPanel/Summer23/{modname}/normal/{modname}_VP{sensor}-{asic}_{param}.csv'
                fdata = np.loadtxt(rpath, delimiter = ',')
                if rows == 'all':
                    pass
                else:
                    fdata = fdata[rows::16]
                x[mod*12 + sensor*3 + asic] = np.nanmean(fdata)#fdata[:, 1::2] - fdata[:, ::2])

    ifbeam = catalog.split('_')[-1]
    plot(x, y, catalog, f'S23_equalisation_{run}_{ifbeam}_{param}_rows{rows}', title = f'Run {run} {ifbeam}', 
         xlabel = param, ylabel = '6-22-38-... rows, aggregated sum of hits')

if __name__ == '__main__':
    #date = datetime.today().strftime('%d%m%Y')
    #print("Starting...")
    daclist = ['VFBK', 'VINCAS', 'VPREAMP_CAS', 'VCASDISC', 'IDISC', 'IPREAMP', 'IPIXELDAC']
    #dac = 'VFBK'
    #print(f'... processing {dac}')
    #if len(sys.argv) == 3:
    #    collect(int(sys.argv[1]), dac, 'VeloMon', f'VPClusterMonitors_{sys.argv[2]}')
    #elif len(sys.argv) == 4:
    #    collect(int(sys.argv[1]), dac, 'VeloMon', f'VPClusterMonitors_{sys.argv[2]}', sys.argv[3])
    #else:
    #    print('... please provide at least the run number, the DAC name, and if the beam (BeamCrossing/NoBeam).')
    #   print('... ... add halfwayreduction to get the comparison of the DAC change.')
    
    for dac in daclist:
        print(f'... making correlation plot for {dac}')
        #dac_vs_hits(293633, 'VPClusterMonitors_BeamCrossing', dac)
        #dac_vs_hits(293633, 'VPClusterMonitors_NoBeam', dac)
        #dac_halfway_vs_hits(293633, 'VPClusterMonitors_BeamCrossing', dac)
        #dac_halfway_vs_hits(293633, 'VPClusterMonitors_NoBeam', dac)
    """
    equal_vs_hits(293633, 'VPClusterMonitors_BeamCrossing', 'scan0_std')
    equal_vs_hits(293633, 'VPClusterMonitors_BeamCrossing', 'scan0_std', 6)
    equal_vs_hits(293633, 'VPClusterMonitors_BeamCrossing', 'scan0_std', 15)
    #equal_vs_hits(293633, 'VPClusterMonitors_NoBeam', 'scan15_std')
    equal_vs_hits(293633, 'VPClusterMonitors_BeamCrossing', 'scan0_rate')
    #equal_vs_hits(293633, 'VPClusterMonitors_NoBeam', 'scan15_rate')
    equal_vs_hits(293633, 'VPClusterMonitors_BeamCrossing', 'scan0_mean')
    #equal_vs_hits(293633, 'VPClusterMonitors_NoBeam', 'scan15_mean')
    equal_vs_hits(293633, 'VPClusterMonitors_BeamCrossing', 'trim_recipe')
    equal_vs_hits(293633, 'VPClusterMonitors_BeamCrossing', 'trim_recipe', 6)
    equal_vs_hits(293633, 'VPClusterMonitors_BeamCrossing', 'trim_recipe', 15)

    equal_vs_hits(293633, 'VPClusterMonitors_NoBeam', 'scan0_std')
    equal_vs_hits(293633, 'VPClusterMonitors_NoBeam', 'scan0_std', 6)
    equal_vs_hits(293633, 'VPClusterMonitors_NoBeam', 'scan0_std', 15)
    #equal_vs_hits(293633, 'VPClusterMonitors_NoBeam', 'scan15_std')
    equal_vs_hits(293633, 'VPClusterMonitors_NoBeam', 'scan0_rate')
    #equal_vs_hits(293633, 'VPClusterMonitors_NoBeam', 'scan15_rate')
    equal_vs_hits(293633, 'VPClusterMonitors_NoBeam', 'scan0_mean')
    #equal_vs_hits(293633, 'VPClusterMonitors_NoBeam', 'scan15_mean')
    equal_vs_hits(293633, 'VPClusterMonitors_NoBeam', 'trim_recipe')
    equal_vs_hits(293633, 'VPClusterMonitors_NoBeam', 'trim_recipe', 6)
    equal_vs_hits(293633, 'VPClusterMonitors_NoBeam', 'trim_recipe', 15)
   """ 


