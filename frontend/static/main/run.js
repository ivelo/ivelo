export const handleActivePageNumber = (currentPage) => {
    document.querySelectorAll(".pagination-number").forEach((button) => {
      button.classList.remove("active");
      const pageIndex = Number(button.getAttribute("page-index"));
      if (pageIndex == currentPage) {
        button.classList.add("active"); }
    });
};

export const appendPageNumber = (index) => {
    const pageNumber = document.createElement("button");
    pageNumber.className = "pagination-number";
    pageNumber.innerHTML = index;
    pageNumber.setAttribute("page-index", index);
    const paginationNumbers = document.getElementById("pagination-numbers");
    paginationNumbers.appendChild(pageNumber);
  };

export const setCurrentPage = (pageNum) => {
    const pageLimit = 10;
    let currentPage = pageNum;

    handleActivePageNumber(pageNum);
    const prevRange = (pageNum - 1) * pageLimit;  
    const currRange = pageNum * pageLimit;           
    const paginatedList = document.getElementById("recipelist");
    const listItems = paginatedList.querySelectorAll("tr.apifilled");
    console.log(listItems);
    
    listItems.forEach((item, index) => {
      //console.log(item, index);       
      item.classList.add("hidden");              
      if (index >= prevRange && index < currRange) {              
        item.classList.remove("hidden");
        //console.log('unhide this');
      }              
    });              
};

export const arrangePagination = (counter) => {
  const pageLimit = 10;
  const nrows = counter;
  const pageCount = Math.ceil(nrows / pageLimit);
  for (let i = 1; i <= pageCount; i++) {
    appendPageNumber(i);
  }
  setCurrentPage(1);
  console.log(document.querySelectorAll(".pagination-number"));
  document.querySelectorAll(".pagination-number").forEach((button) => {
      console.log('Clicked a button');
      const pageIndex = Number(button.getAttribute("page-index"));
      if (pageIndex) {
          button.addEventListener("click", () => {
              setCurrentPage(pageIndex);
          });
      }
  });
};

export function buildrundb(detcat, runnumber){
  const csrftoken = document.querySelector('[name=csrfmiddlewaretoken]').value;
  var runsuffix = "";
  if (runnumber != -1) runsuffix = '&runid=' + runnumber;
  $.ajax({
    url: 'http://10.128.124.243:8003/api/run?partitionname=' + detcat + runsuffix,
    type: 'GET',
    dataType: 'json',
    headers: {'X-CSRFToken':  csrftoken},
    crossDomain: true,
    crossOrigin: true,
    success: function(data, textStatus) {
        var boxfill = "<tr>" +
        "<th><div style='width:90px'>Analysed</div></th>" +
        "<th><div style='width:90px'>Run</div></th>" +
        "<th><div style='width:90px'>Run type</div></th>" +
        "<th><div style='width:90px'>Activity</div></th>" +
        "<th><div style='width:180px'>Start time</div></th>" +
        "<th><div style='width:180px'>End time</div></th>" +
        "<th><div style='width:90px'>av. mu</div></th>" +
        "<th><div style='width:310px'>Comment</div></th></tr>";
        let counter = 0;
        for (var item in data) {
            
            //if (counter == 0) boxfill += "<tr class = 'apifilled recipelist-selected'>";
            //else boxfill += "<tr class = 'apifilled'>";
            //if (counter != 0) 
            boxfill += "<tr class = 'apifilled'>";
            //else boxfill += "<tr class = 'apifilled recipelist-selected'>";

            var item_obj = data[item];
            //boxfill += " <td><div style='width:40px;'><button style='width:40px;padding:0x;text-align:center;height:16px;font-size:10px;'>Add</button></div> </td>";
            console.log(item_obj);
            if (item_obj['starttime'] != null)
              item_obj['starttime'] = item_obj['starttime'].replace('T', ' ').replace(/\+.*/, "");
            if (item_obj['endtime'] != null)
              item_obj['endtime'] = item_obj['endtime'].replace('T', ' ').replace(/\+.*/, "");
            boxfill += " <td><div> " + item_obj['analysed'] + "</div> </td>";
            boxfill += " <td><div> " + item_obj['runid'] + "</div> </td>";
            boxfill += " <td><div> " + item_obj['runtype'] + "</div> </td>";
            boxfill += " <td><div> " + item_obj['activity'] + "</div> </td>";
            boxfill += " <td><div> " + item_obj['starttime'] + "</div> </td>";
            boxfill += " <td><div> " + item_obj['endtime'] + "</div> </td>";
            boxfill += " <td><div> " + item_obj['avMu'] + "</div> </td>";
            boxfill += " <td><div> " + item_obj['comment'] + "</div> </td>";
            boxfill += "</tr>"
            counter++;
        }
        if (counter == 0) boxfill += "<td><div>No run matching criteria.</div></td>"
        document.getElementById("recipelist").innerHTML = boxfill;
        //document.querySelectorAll('.recipelist tr').forEach( e => e.addEventListener("click", function() {
        //  if (which == 'hwrecipe') getChipRecipeConfig(e.childNodes[0].innerText, '0');
        //}));
        arrangePagination(counter);
        $(".recipelist tr").click(function(){
          console.log($(this));
          var runnumber = $(this);//.childNodes[1];
          console.log('Runumber should be ' + runnumber);
          $(this).addClass('recipelist-selected').siblings().removeClass('recipelist-selected');
          $.ajax({
            url: 'http://10.128.124.243:8003/api/run?partitionname=' + detcat + '&runid=' + runnumber,
            type: 'GET',
            dataType: 'json',
            headers: {'X-CSRFToken':  csrftoken},
            crossDomain: true,
            crossOrigin: true,
            success: function(data, textStatus) {},
          });
          item_obj = data[0];
          document.getElementById("runlabel").innerHTML = item_obj['runid']
          document.getElementById("partitionlabel").innerHTML = item_obj['partitionname']
          document.getElementById("runtypelabel").innerHTML = item_obj['runtype']
          document.getElementById("destinationlabel").innerHTML = item_obj['destination']
          document.getElementById("activitylabel").innerHTML = item_obj['activity']
          //document.getElementById("partitionlabel").innerHTML = item_obj['partitionname']
          //$(this).addClass('recipelist-selected').siblings().removeClass('recipelist-selected');
          //var event = new Event('change');
          // Dispatch it.
          //document.getElementById('param_select').dispatchEvent(event);
          //document.getElementById('param_select').onchange();
        });
    },
});
};

export function clearmap(){
  var tablecells = document.querySelectorAll(".asiccell");
  for (var i = 0; i < tablecells.length; i++){
    tablecells[i].innerHTML = "";
    tablecells[i].style.removeProperty('background');
  }
};